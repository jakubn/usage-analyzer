# Architecture

While we do not know the RPS, I assume that we need to get ready for large traffic (these types of businesses are low margin).

So in essence we have a high RPS and medium availability requirements. The easiest way to provide that is to:
- separate changes into different deployments so that unrelated changes do not impact availability for other functionalities (availability is inversely proportional to frequency of change)
- separate functionalities so that spikes in RPS do not impact different processes (it's easy to provide high RPS if you limit the processing to single function)

This implies using distributed system and/or lambdas. Since this is low margin business model, lambdas are not an option.

With distributed system we could go with "slightly distributed" (a few apps) or true microservices (where the time it 
takes to create a new microservice based on a tailored template is 4.5 seconds - including pipeline to prod, etc.).
I assume microservices.

For internal communication I choose Kafka, but it could be any persisted distributed transaction log 
(Apache Pulsar, AWS Kinesis, etc.) - this protects us against internal DDOS (communication is pull based),
gives us auditable data in between (no Envers/Javers necessary for debugging/audit), improves security 
(no direct communication between pods) and flows well with async
model required for minimal impact of failure. By using user id as the partition key in "system used" topic, we also 
solve the problem of optimistic locking in usage analyzer DB - every user usage is processed in a single thread.

The architecture is as follows:

![](structurizr-Containers.png)

# Implementation

The task was to implement 3 types of API endpoints

> Create REST API endpoints exposing system’s functionalities (the implementation could be naive, or even mocked)

Since these were about to be mocked I assume to goal was to verify the architecture. You have it already, so we can skip it.

> Create a REST API endpoint to retrieve the usage data for a specific customer, filtered by a date range.

> Create a REST API endpoint to retrieve the usage data for all customers, aggregated by day/week/month.

These belong to Usage Analyzer microservice, so I'm implementing that.

Technical requirement was to use Java and postgresql. I'd prefer reactive programming for this task because optimizing 
would be easier (no tomcat/hikari thread pools to customize), but that requires a reactive DB driver to give benefits.
Postgresql has such a driver and even hibernate 6 supports reactive communication (via Vert.x) so we could go either
with R2DBC or Hibernate 6 reactive, but I don't have prod experience with both, which means I'd need to do stress tests 
to compare them. That's a bit much for a homework. 
I'd go with reactive MongoDB, but that's against tech requirements, so I'm sticking with imperative implementation 
instead. Mind you that it means stress testing and fine tuning of tomcat & hikari thread pools before going to prod 
(and on every deploy to prod).

Evaluation criteria mention 'appropriate database relationships'. We don't need 3NF/5NF 
(or any relations for that matter), as they do not protect us against anything in this case (data duplication is ok,
there is a single writer, size is not an issue), and any joins would make us slower.
We could go with CQRS and a separate query-model instead, but again, we don't get anything if we know our usage-patterns 
ahead, and from the task it seems we do. 
Day/week/month usage could be calculated once via a scheduler and stored if many calls are expected.

Observability is provided by actuator/micrometer (tech metrics), which also covers business metrics 
(1 to 1 with REST calls). Additional logging only on code branches.

I'd base public authentication of this system on JWT, but since UsageAnalyzer does not have public endpoints, 
I don't need to care about it for now.   

Production ready system should get a DLQ, dashboards, tracing (Rest -> Kafka), pipelines with static code analysis.

# How to run it

Prerequisites:
- JDK 19
- Docker installed (if on M1/M2, set the Rosetta 2 support on)

Run ```gradle test```

This fires up Kafka, Postgresql and runs all the tests

# How to run it on production

Ah, you want a pipeline? A docker image? Where's my production then?

# How to run it locally

What for? Testing is automated and there's no UI.

But if you really want, create a `local` profile and set DB + Kafka brokers urls. 
There are also defaults, so if your Kafka/Postgresql are on standard ports, it might work out of the box.
Topics are on you, though.

# Structurizr C4 DSL

```
workspace "Photo Storage and Compute" "Photo Storage and Compute" {

    model {
        enterprise "Photo Storage and Compute" {
            customer = person "Customer" "Customer" "Customer"


            photoStorageSystem = softwaresystem "Photo Storage"  {
                photoSaver = container "Photo Saver" "Saves photo"
                s3 = container "S3 or other filesystem/bucket"
                thumbnailGenerator = container "Thumbnail generator" "Generates thumbnails"
                usageAnalyzer = container "Usage Analyzer" "Counts used CPU/Storage"
                photoViewer = container "Photo Viewer" "Shows photos"
                aiAnalyzer = container "AI Analyzer" "Photo AI analyzer/tagger"
                searcher = container "Photo searcher" "Allows to search photos by tags and metadata" "Elasticsearch or similar"
                systemUsedTopic = container "Topic: system used" "Events: System Used; partition key: username" "Kafka topic" "Topic"
                uploadedPhotosTopic = container "Topic: uploaded Photos" "Events: photo uploaded; partitioning: random" "Kafka topic" "Topic"
                createdTagsTopic = container "Topic: created tags"  "Events: tag created; partitioning by photo id" "Kafka topic" "Topic"
            }
        }
        
        invoicingSystem = softwaresystem "Invoicing System" "Invoice creation" "Existing System"

        # relationships between people and software systems
        customer -> photoSaver "save photo" "HTTP PUT"
        customer -> photoViewer "get photo" "HTTP GET"
        customer -> photoViewer "list photo" "HTTP GET"
        photoSaver -> s3 "store photo"
        thumbnailGenerator -> s3 "get photo"
        thumbnailGenerator -> s3 "store thumbnail"
        photoSaver -> systemUsedTopic "system used"
        photoSaver -> uploadedPhotosTopic "photo uploaded"
        uploadedPhotosTopic -> thumbnailGenerator "photo uploaded"
        uploadedPhotosTopic -> photoViewer "photo uploaded"
        uploadedPhotosTopic -> aiAnalyzer  "photo uploaded"
        uploadedPhotosTopic -> searcher  "photo uploaded"
        photoViewer -> s3 "get photo"
        photoViewer -> s3 "get thumbnail"
        aiAnalyzer -> s3 "get photo"
        photoViewer -> systemUsedTopic "system used"
        systemUsedTopic -> usageAnalyzer "system used"
        invoicingSystem -> usageAnalyzer "Get usage per day for user" "HTTP GET"
        invoicingSystem -> usageAnalyzer "Get usage per month" "HTTP GET"
        invoicingSystem -> usageAnalyzer "Get usage per year" "HTTP GET"
        invoicingSystem -> usageAnalyzer "Get usage per week" "HTTP GET"
        customer -> searcher "Find photo" "HTTP GET" 
        aiAnalyzer -> createdTagsTopic "Tag created"
        createdTagsTopic -> searcher "Tag created"
    }

    views {

        container photoStorageSystem "Containers" {
            include *
            autoLayout
        }


        styles {
            element "Customer" {
                background #08427b
                shape Person
            }
            element "Topic" {
                background #1168bd
                shape Pipe
            }
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "Existing System" {
                background #999999
                color #ffffff
            }
            element "Container" {
                background #438dd5
                color #ffffff
            }
            element "Mobile App" {
                shape MobileDeviceLandscape
            }
            element "Component" {
                background #85bbf0
                color #000000
            }
        }
    }
}
```
