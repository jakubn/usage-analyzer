create table user_usage_per_day
(
    username             varchar(255) not null,
    day                  date not null,
    compute_used         int4 not null,
    storage_used         int4 not null,
    version              int4 not null,
    primary key (day, username)
);

-- If calls for aggregated values are common
-- we should store the outcomes and not calculate them again
--
-- create table usage_per_day
-- (
--     day                  date not null,
--     compute_used         int4 not null,
--     storage_used         int4 not null,
--     primary key (day)
-- );
--
-- create table usage_per_week
-- (
--     year                 int4 not null,
--     week                 int4 not null,
--     compute_used         int4 not null,
--     storage_used         int4 not null,
--     primary key (year, week)
-- );
--
-- create table usage_per_year
-- (
--     year                 int4 not null,
--     month                int4 not null,
--     compute_used         int4 not null,
--     storage_used         int4 not null,
--     primary key (year, month)
-- );
