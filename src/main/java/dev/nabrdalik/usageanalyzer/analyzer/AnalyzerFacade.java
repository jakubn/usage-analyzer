package dev.nabrdalik.usageanalyzer.analyzer;

import dev.nabrdalik.usageanalyzer.analyzer.dto.SystemUsedEvent;
import dev.nabrdalik.usageanalyzer.analyzer.dto.UsagePerDayDto;
import dev.nabrdalik.usageanalyzer.analyzer.dto.UserUsagePerDayDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.function.Function;

@AllArgsConstructor
@Slf4j
public class AnalyzerFacade {
    public static final String SYSTEM_USED_TOPIC_NAME = "system-used";
    public static final String KAFKA_GROUP_ID = "usage-analyzer";

    private final UserUsagePerDayRepository userUsagePerDayRepository;

    @Transactional
    @KafkaListener(topics = SYSTEM_USED_TOPIC_NAME, groupId = KAFKA_GROUP_ID)
    public void systemUsed(SystemUsedEvent systemUsedEvent) {
        UserUsagePerDay.UserUsageId id = createId(systemUsedEvent);
        UserUsagePerDay userUsagePerDay = userUsagePerDayRepository.findById(id)
                .map(updateExistingUsage(systemUsedEvent))
                .orElseGet(() -> createUserUsageForDay(systemUsedEvent));
        userUsagePerDayRepository.saveAndFlush(userUsagePerDay);
    }

    private static Function<UserUsagePerDay, UserUsagePerDay> updateExistingUsage(SystemUsedEvent systemUsedEvent) {
        return usageSoFar -> {
            log.info("Updating existing usage {} with event {}", usageSoFar, systemUsedEvent);
            return usageSoFar.add(systemUsedEvent);
        };
    }

    private UserUsagePerDay createUserUsageForDay(SystemUsedEvent systemUsedEvent) {
        log.info("Creating first usage for user and day {}", systemUsedEvent);
        return new UserUsagePerDay(createId(systemUsedEvent), systemUsedEvent);
    }

    public UserUsagePerDayDto getUsage(String username, LocalDate utcDate) {
        UserUsagePerDay.UserUsageId id = createId(username, utcDate);
        return userUsagePerDayRepository.findById(id)
                .map(UserUsagePerDay::dto)
                .orElseGet(() -> new UserUsagePerDayDto(username, 0, 0));
    }

    private UserUsagePerDay.UserUsageId createId(SystemUsedEvent systemUsedEvent) {
        LocalDate utcDate = getUtcDate(systemUsedEvent.time());
        return createId(systemUsedEvent.username(), utcDate);
    }

    private LocalDate getUtcDate(Instant photoViewedEvent) {
        return photoViewedEvent
                .atZone(ZoneOffset.UTC)
                .toLocalDate();
    }

    private UserUsagePerDay.UserUsageId createId(String username, LocalDate utcDate) {
        return new UserUsagePerDay.UserUsageId(username, utcDate);
    }

    public UsagePerDayDto getUsageForDay(LocalDate utcDay) {
        SumForDay sumForDay = userUsagePerDayRepository.summarizeForDay(utcDay);
        return new UsagePerDayDto(sumForDay.getStorageUsed(), sumForDay.getComputeUsed(), utcDay);
    }
}
