package dev.nabrdalik.usageanalyzer.analyzer;

import dev.nabrdalik.usageanalyzer.analyzer.dto.UserUsagePerDayDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

interface UserUsagePerDayRepository extends Repository<UserUsagePerDay, UserUsagePerDay.UserUsageId> {
    void saveAndFlush(UserUsagePerDay userUsagePerDay);
    Optional<UserUsagePerDay> findById(UserUsagePerDay.UserUsageId userUsageId);

    @Query("select new dev.nabrdalik.usageanalyzer.analyzer.SumForDay(sum(u.storageUsed), sum(u.computeUsed)) " +
            "from user_usage_per_day u where u.day = :utcDay")
    SumForDay summarizeForDay(LocalDate utcDay);
}

@Getter
@AllArgsConstructor
class SumForDay {
    private Long storageUsed = 0L;
    private Long computeUsed = 0L;

    void add(UserUsagePerDay usage) {
        UserUsagePerDayDto dto = usage.dto();
        storageUsed += dto.size();
        computeUsed += dto.compute();
    }

    void add(SumForDay acc2) {
        storageUsed += acc2.storageUsed;
        computeUsed += acc2.computeUsed;
    }
}

class InMemoryUserUsagePerDayRepository implements UserUsagePerDayRepository {
    private final Map<UserUsagePerDay.UserUsageId, UserUsagePerDay> store = new ConcurrentHashMap<>();
    @Override
    public void saveAndFlush(UserUsagePerDay userUsagePerDay) {
        store.put(new UserUsagePerDay.UserUsageId(userUsagePerDay.getUsername(), userUsagePerDay.getDay()), userUsagePerDay);
    }

    @Override
    public Optional<UserUsagePerDay> findById(UserUsagePerDay.UserUsageId userUsageId) {
        return Optional.ofNullable(store.get(userUsageId));
    }

    @Override
    public SumForDay summarizeForDay(LocalDate utcDay) {
        //java still doesn't have leftFold? sigh...
        return store.values()
                .stream()
                .filter(userUsagePerDay -> userUsagePerDay.getDay().isEqual(utcDay))
                .collect(
                        () -> new SumForDay(0L, 0L),
                        SumForDay::add,
                        SumForDay::add
                );
    }
}


