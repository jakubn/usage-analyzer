package dev.nabrdalik.usageanalyzer.analyzer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class AnalyzerConfiguration {
    AnalyzerFacade analyzerFacade() {
        UserUsagePerDayRepository userUsagePerDayRepository = new InMemoryUserUsagePerDayRepository();
        return analyzerFacade(userUsagePerDayRepository);
    }

    @Bean
    AnalyzerFacade analyzerFacade(UserUsagePerDayRepository userUsagePerDayRepository) {
        return new AnalyzerFacade(userUsagePerDayRepository);
    }
}
