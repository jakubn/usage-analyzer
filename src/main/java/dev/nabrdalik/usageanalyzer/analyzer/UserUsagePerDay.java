package dev.nabrdalik.usageanalyzer.analyzer;

import dev.nabrdalik.usageanalyzer.analyzer.dto.SystemUsedEvent;
import dev.nabrdalik.usageanalyzer.analyzer.dto.UserUsagePerDayDto;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.time.LocalDate;

@Entity(name="user_usage_per_day")
@IdClass(UserUsagePerDay.UserUsageId.class)
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
class UserUsagePerDay {
    @Id
    @Getter
    String username;

    @Id
    @Getter
    LocalDate day;

    @Column(name = "compute_used")
    Integer computeUsed;
    @Column(name = "storage_used")
    Integer storageUsed;
    @Version
    Integer version;

    UserUsagePerDay(UserUsageId id, SystemUsedEvent systemUsedEvent) {
        username = id.username;
        day = id.day;
        computeUsed = systemUsedEvent.compute();
        storageUsed = systemUsedEvent.size();
    }

    UserUsagePerDay add(SystemUsedEvent systemUsedEvent) {
        this.storageUsed += systemUsedEvent.size();
        this.computeUsed += systemUsedEvent.compute();
        return this;
    }

    UserUsagePerDayDto dto() {
        return new UserUsagePerDayDto(username, storageUsed, computeUsed);
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @EqualsAndHashCode
    static class UserUsageId implements Serializable {
        private String username;
        private LocalDate day;
    }
}
