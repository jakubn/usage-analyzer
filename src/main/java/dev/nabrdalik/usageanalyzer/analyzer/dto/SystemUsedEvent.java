package dev.nabrdalik.usageanalyzer.analyzer.dto;

import java.time.Instant;

public record SystemUsedEvent(
        String username,
        int size,
        int compute,
        Instant time,
        String photoPath
) {}
