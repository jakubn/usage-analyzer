package dev.nabrdalik.usageanalyzer.analyzer.dto;

public record UserUsagePerDayDto(
        String username,
        int size,
        int compute
) {
}
