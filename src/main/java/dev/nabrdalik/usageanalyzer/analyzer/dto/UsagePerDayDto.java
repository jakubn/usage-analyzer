package dev.nabrdalik.usageanalyzer.analyzer.dto;

import java.time.LocalDate;

public record UsagePerDayDto(
        Long size,
        Long compute,
        LocalDate day
) { }
