package dev.nabrdalik.usageanalyzer.analyzer.infrastructure.web;

import dev.nabrdalik.usageanalyzer.analyzer.AnalyzerFacade;
import dev.nabrdalik.usageanalyzer.analyzer.dto.UsagePerDayDto;
import dev.nabrdalik.usageanalyzer.analyzer.dto.UserUsagePerDayDto;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequestMapping("/usage")
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor
class UsageController {
    AnalyzerFacade analyzerFacade;

    @GetMapping("/{day}/{username}")
    UserUsagePerDayDto userUsagePerDay(@NonNull @PathVariable String username, @NonNull @PathVariable LocalDate day) {
        return analyzerFacade.getUsage(username, day);
    }

    @GetMapping("/{day}")
    UsagePerDayDto userUsagePerDay(@NonNull @PathVariable LocalDate day) {
        return analyzerFacade.getUsageForDay(day);
    }
}
