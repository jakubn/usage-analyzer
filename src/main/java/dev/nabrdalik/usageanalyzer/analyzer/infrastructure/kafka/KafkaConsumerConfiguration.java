package dev.nabrdalik.usageanalyzer.analyzer.infrastructure.kafka;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.support.converter.JsonMessageConverter;

@Configuration
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
class KafkaConsumerConfiguration {

    //In a normal system I'd add DLQ on error, and dashboards for it
    //but without access to production it's pointless (why DLQ if you don't have dashboards)
    @Bean
    JsonMessageConverter kafkaMessageConverter() {
        return new JsonMessageConverter();
    }
}
