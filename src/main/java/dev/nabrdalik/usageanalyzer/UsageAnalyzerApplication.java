package dev.nabrdalik.usageanalyzer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@EnableKafka
@SpringBootApplication
public class UsageAnalyzerApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsageAnalyzerApplication.class, args);
	}

}
