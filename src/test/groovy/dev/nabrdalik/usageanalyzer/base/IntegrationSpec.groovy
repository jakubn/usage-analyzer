package dev.nabrdalik.usageanalyzer.base

import dev.nabrdalik.usageanalyzer.UsageAnalyzerApplication
import dev.nabrdalik.usageanalyzer.analyzer.AnalyzerFacade
import dev.nabrdalik.usageanalyzer.analyzer.dto.SystemUsedEvent
import groovy.transform.TypeChecked
import org.apache.kafka.clients.admin.AdminClientConfig
import org.apache.kafka.clients.admin.NewTopic
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.serialization.StringSerializer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.kafka.config.TopicBuilder
import org.springframework.kafka.core.DefaultKafkaProducerFactory
import org.springframework.kafka.core.KafkaAdmin
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.core.ProducerFactory
import org.springframework.kafka.support.serializer.JsonSerializer
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.transaction.annotation.Transactional
import org.testcontainers.containers.KafkaContainer
import org.testcontainers.spock.Testcontainers
import org.testcontainers.utility.DockerImageName
import spock.lang.Specification

@TypeChecked
@SpringBootTest
@ContextConfiguration(classes = [UsageAnalyzerApplication, TestKafkaConfiguration]) //current version of spock doesn't work with @SpringBootTest in Spring 6 alone
@AutoConfigureMockMvc
@ActiveProfiles("integration")
@Transactional
@Testcontainers
abstract class IntegrationSpec extends Specification {

    static final KafkaContainer kafka

    static {
        kafka = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:6.2.1"))
        kafka.start()
        String brokers = kafka.getBootstrapServers()
        System.setProperty("spring.kafka.bootstrap-servers", brokers)
    }

    @Autowired
    MockMvc mockMvc
}

@TestConfiguration
@TypeChecked
class TestKafkaConfiguration {
    @Bean
    KafkaAdmin admin() {
        Map<String, Object> configs = new HashMap<>()
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, IntegrationSpec.kafka.getBootstrapServers())
        return new KafkaAdmin(configs)
    }

    @Bean
    ProducerFactory<String, SystemUsedEvent> systemUsedProducerFactory() {
        Map<String, Object> configs = new HashMap<>()
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, IntegrationSpec.kafka.getBootstrapServers())
        configs.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configs.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        return new DefaultKafkaProducerFactory<>(configs);
    }

    @Bean
    KafkaTemplate<String, SystemUsedEvent> systemUsedKafkaTemplate(ProducerFactory<String, SystemUsedEvent>  systemUsedProducerFactory) {
        return new KafkaTemplate<>(systemUsedProducerFactory, true);
    }

    @Bean
    NewTopic systemUsedTopic() {
        return TopicBuilder.name(AnalyzerFacade.SYSTEM_USED_TOPIC_NAME)
                .partitions(1)
                .replicas(1)
                .build()
    }
}
