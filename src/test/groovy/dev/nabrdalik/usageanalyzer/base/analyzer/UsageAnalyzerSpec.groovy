package dev.nabrdalik.usageanalyzer.base.analyzer

import dev.nabrdalik.usageanalyzer.analyzer.AnalyzerConfiguration
import dev.nabrdalik.usageanalyzer.analyzer.AnalyzerFacade
import dev.nabrdalik.usageanalyzer.analyzer.dto.UsagePerDayDto
import dev.nabrdalik.usageanalyzer.analyzer.dto.UserUsagePerDayDto
import spock.lang.Specification

import java.time.Instant
import java.time.temporal.ChronoUnit

class UsageAnalyzerSpec extends Specification implements SamplePhotos {
    AnalyzerFacade analyzerFacade = new AnalyzerConfiguration().analyzerFacade()

    def "should calculate usage per user per day"() {
        given: "user uploaded a photo"
            analyzerFacade.systemUsed(systemUsedEvent(photo1Size, photo1Compute))

        and: "user viewed a photo or thumbnail"
            analyzerFacade.systemUsed(systemUsedEvent(photo1Size))

        and: "uploaded another photo"
            analyzerFacade.systemUsed(systemUsedEvent(photo2Size, photo2Compute))

        when: "invoicing asks for usage per user per day"
            UserUsagePerDayDto userUsagePerDay = analyzerFacade.getUsage(username, today)

        then: "usage is returned"
            with(userUsagePerDay) {
                size == photo1Size * 2 + photo2Size
                compute == photo1Compute + photo2Compute
            }
    }

    def "should return 0 when user didn't use the service given day"() {
        given: "user didn't use the service today"

        when: "invoicing asks for usage per user per day"
            UserUsagePerDayDto userUsagePerDay = analyzerFacade.getUsage(username, today)

        then: "usage is zero"
            with(userUsagePerDay) {
                size == 0
                compute == 0
            }
    }

    def "should calculate usage per day"() {
        given: "we have 4 uploads, one before, 2 today from different users, one after today"
            systemUsed(photo1Size, photo1Compute, now.minus(1, ChronoUnit.DAYS), username)
            systemUsed(photo2Size, photo2Compute, now, username)
            systemUsed(photo1Size, photo1Compute, now, username2)
            systemUsed(photo2Size, photo2Compute, now.plus(1, ChronoUnit.DAYS), username2)

        when: "invoicing asks for last day usage"
            UsagePerDayDto usage = analyzerFacade.getUsageForDay(today)

        then: "calculated usage for 2 uploads in the middle is returned"
            with(usage) {
                size == photo1Size + photo2Size
                compute == photo1Compute + photo2Compute
                day == today
            }
    }

    private void systemUsed(int size, int compute, Instant when, String byWhom) {
        analyzerFacade.systemUsed(systemUsedEvent(size, compute, when, byWhom))
    }

    // These two are so similar to usage per day (just slightly different jpql), so I'm skipping this

    def "should calculate usage per week"() {
    }

    def "should calculate usage per month"() {
    }
}
