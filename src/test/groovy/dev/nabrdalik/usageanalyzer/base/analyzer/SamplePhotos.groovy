package dev.nabrdalik.usageanalyzer.base.analyzer

import dev.nabrdalik.usageanalyzer.analyzer.dto.SystemUsedEvent
import groovy.transform.SelfType
import groovy.transform.TypeChecked
import spock.lang.Specification

import java.time.Instant
import java.time.LocalDate
import java.time.ZoneOffset
import java.time.temporal.ChronoUnit

@TypeChecked
@SelfType(Specification)
trait SamplePhotos {
    String username = "Johnny"
    String username2 = "Mnemonic"
    int photo1Size = 10
    int photo1Compute = 7
    int photo2Size = 5
    int photo2Compute = 3
    Instant now = Instant.now()
    Instant longAgo = Instant.now().minus(100, ChronoUnit.DAYS)
    LocalDate today = now.atZone(ZoneOffset.UTC).toLocalDate();
    String photoPath = "path"

    SystemUsedEvent systemUsedEvent(int size, int compute = 0, Instant when = now, String username = this.username) {
        return new SystemUsedEvent(username, size, compute, when, photoPath)
    }
}
