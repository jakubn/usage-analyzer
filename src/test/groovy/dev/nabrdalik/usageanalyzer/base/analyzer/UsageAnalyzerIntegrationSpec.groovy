package dev.nabrdalik.usageanalyzer.base.analyzer

import dev.nabrdalik.usageanalyzer.analyzer.AnalyzerFacade
import dev.nabrdalik.usageanalyzer.analyzer.dto.SystemUsedEvent
import dev.nabrdalik.usageanalyzer.base.IntegrationSpec
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate

import java.time.Instant
import java.time.ZoneOffset
import java.time.temporal.ChronoUnit

import static dev.nabrdalik.usageanalyzer.analyzer.AnalyzerFacade.SYSTEM_USED_TOPIC_NAME
import static dev.nabrdalik.usageanalyzer.base.pooling.PredefinedPollingConditions.WAIT
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class UsageAnalyzerIntegrationSpec extends IntegrationSpec implements SamplePhotos {
    @Autowired
    AnalyzerFacade analyzerFacade

    @Autowired
    KafkaTemplate<String, SystemUsedEvent> kafkaTemplate

    def "should calculate usage per user per day"() {
        given: "user uploaded a photo"
            send(systemUsedEvent(photo1Size, photo1Compute))

        and: "user viewed a photo or thumbnail"
            send(systemUsedEvent(photo1Size))

        expect: "usage is calculated"
            WAIT.eventually {
                mockMvc.perform(get("/usage/${today}/${username}"))
                        .andExpect(status().isOk())
                        .andExpect(content().json("""
                              {
                                "size": ${photo1Size * 2},
                                "compute": ${photo1Compute} 
                              }                
                             """))
            }
    }

    def "should calculate usage per day"() {
        given: "we have 4 uploads, one before, 2 today from different users, one after today"
            send(photo1Size, photo1Compute, longAgo.minus(1, ChronoUnit.DAYS), username)
            send(photo2Size, photo2Compute, longAgo, username)
            send(photo1Size, photo1Compute, longAgo, username2)
            send(photo2Size, photo2Compute, longAgo.plus(1, ChronoUnit.DAYS), username2)

        expect: "calculated usage for 2 uploads in the middle is returned to invoicing"
            WAIT.eventually {
                mockMvc.perform(get("/usage/${longAgo.atZone(ZoneOffset.UTC).toLocalDate()}"))
                        .andExpect(status().isOk())
                        .andExpect(content().json("""
                              {
                                "size": ${photo1Size + photo2Size},
                                "compute": ${photo1Compute + photo2Compute}
                              }                
                             """))
            }
    }

    private void send(SystemUsedEvent event) {
        kafkaTemplate.send(SYSTEM_USED_TOPIC_NAME, username, event)
    }

    private void send(int size, int compute, Instant when, String username) {
        kafkaTemplate.send(SYSTEM_USED_TOPIC_NAME, username, systemUsedEvent(size, compute, when, username))
    }

}
